# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=swayr
pkgver=0.12.0
pkgrel=0
pkgdesc="A window switcher (and more) for Sway"
url="https://sr.ht/~tsdh/swayr/"
arch="aarch64 armhf armv7 ppc64le x86 x86_64"  # limited by rust/cargo
license="GPL-3.0-or-later"
makedepends="cargo"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~tsdh/swayr/archive/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare

	# Optimize binary for size.
	cat >> Cargo.toml <<-EOF

		[profile.release]
		codegen-units = 1
		lto = true
		opt-level = "z"
		panic = "abort"
	EOF

	cargo fetch --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	cargo install --locked --offline --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*
}

sha512sums="
dc0c80634046027ecccd35218874300ddb10f17f3188219bd5605a8e0f4c721b2e912360fdd420fe99d1aa17bd067c4af85c7ef45e19c2cd1e355235fd2df0b7  swayr-0.12.0.tar.gz
"
